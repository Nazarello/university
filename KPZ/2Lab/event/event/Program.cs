﻿using System;
using static Foo;

public class MyEventArgs
{
    public MyEventArgs(int arg) { Counter = arg; }
    public int Counter { get; private set; }
}

class Foo
{
    private int _counter;

    public int Counter
    {
        get
        {
            return _counter;
        }
        set
        {
            _counter = value;

            if (value % 5 == 0)
                if (OnFifteen != null)
                    OnFifteen(this, new MyEventArgs(value));
        }
    }

    public delegate void MyEventHandler(object sender, MyEventArgs e);

    public event MyEventHandler OnFifteen;
}

class Program
{
    static void Main()
    {
        var foo = new Foo { Counter = 10 };
        foo.OnFifteen += (object o, MyEventArgs arg) =>
        Console.WriteLine("{0} is dividable on 5 {0}",
        arg.Counter);

        for (var i = 1; i <= 120; i++)
        {
            Console.WriteLine("foo.Counter = {0}", ++foo.Counter);

            if (i == 100)
                foo.OnFifteen -= (object o, MyEventArgs arg) =>
        Console.WriteLine("{0} is dividable on 5 {0}",
        arg.Counter);
        }

        Console.ReadLine();
    }
}