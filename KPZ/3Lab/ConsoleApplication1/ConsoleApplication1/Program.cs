﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{

    enum Continents
    { Africa, Europe, Asia,America, Australia };

    static class ExtendMethod
    {
        public static IEnumerable<University> IsUkraine(this IEnumerable<University> items)
        {
            foreach (var value in items)
            {
                if (value.UnLocation.country == "Ukraine")
                    yield return value;
            }
        }
    }

    class University : IComparer<University>
    {

        public List<University> List
        {
            get
            {
                return new List<University> 
            {
                new University("Lviv Polytechnic \t",31000,1819,new Location(Continents.Europe,"Ukraine","Lviv")),
                new University("Kyiv Polytechnic Institute",36000,1898,new Location(Continents.Europe,"Ukraine","Kiev")),
                new University("Harvard University \t",21000,1636 ,new Location(Continents.America,"US","Cambridge, Massachusetts")),
                new University("Stanford University \t",16000,1891,new Location(Continents.America,"US","Stanford")),
                new University("University of Cambridge\t",20000,1209,new Location(Continents.Europe,"United Kingdom","Cambridge")),
                new University("University of Oxford\t",23000,1096,new Location(Continents.Europe,"United Kingdom","Oxford")),
                new University("University of Tokyo\t",29000,1877,new Location(Continents.Asia,"Japan","Tokyo")),
                new University("Peking University \t",30000,1898,new Location(Continents.Asia,"China","Beijing ")),
                new University("Australian National University",20000,1946,new Location(Continents.Australia,"Australian ","Acton, Australian Capital Territory"))
            };
            }
        }
        public University(string name,int studentNumber,int year,Location location)
        {
            Name = name;
            StudentNumber = studentNumber;
            Year = year;
            UnLocation = location;
        }

        public University()
        {
        }

        public string Name { get; set; }
        public int StudentNumber { get; set; }
        public int Year { get; set; }
        public Location UnLocation { get; set; }

        public int Compare(University x, University y)
        {
            if (x.StudentNumber < y.StudentNumber)
                return 1;
            if (x.StudentNumber > y.StudentNumber)
                return -1;
            else return 0;
        }
    }

    class Location
    {
        public Location(Continents continent, string country,string city)
        {
            this.continent = continent;
            this.country = country;
            this.city = city;
        }
        public Continents continent;
        public string country;
        public string city;


    }

    class UniversityComperer : IComparer<University>
    {
        public int Compare(University x, University y)
        {
            if (x.Year < y.Year)
                return 1;
            if (x.Year > y.Year)
                return -1;
            else return 0;
        }
    }

    class UniversityCompererByLenght : IComparer<University>
    {
        public int Compare(University x, University y)
        {
            if (x.UnLocation.continent.ToString().Length < y.UnLocation.continent.ToString().Length)
                return 1;
            if (x.UnLocation.continent.ToString().Length > y.UnLocation.continent.ToString().Length)
                return -1;
            else return 0;
        }
    }

    class Program
    {
        private static void Print(List<University>list)
        {
            foreach (var item in list)
            {
                Console.WriteLine("{0}\t {1}\t{2}\t:{3} {4} {5}", item.Name, item.StudentNumber, item.Year, item.UnLocation.continent, item.UnLocation.country, item.UnLocation.city);
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    
        private static void Print(IEnumerable array)
        {
            foreach (var item in array)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static void Main(string[] args)
        {
            List<University> list = new University().List;
            
            
            Print(list);
            var tmp = list.Select(x => x.Name);
            Print(tmp);
            var tmp1 = from x in list
                       where x.StudentNumber > 25000
                       select x;
            Console.WriteLine();
            Print(tmp1.ToList());

            list.Sort(new UniversityCompererByLenght());
            Print(list);

            //var array = list.ToArray();

            var temp = from x in list
                       group x by x.UnLocation.continent.ToString() into newList
                       orderby newList.Key
                       select newList;

            foreach (var un in temp)
            {
                Console.WriteLine("Key: {0}", un.Key);
                foreach (var item in un)
                {
                    Console.WriteLine("\t{0}\t {1}\t{2}\t:{3} {4} {5}", item.Name, item.StudentNumber, item.Year, item.UnLocation.continent, item.UnLocation.country, item.UnLocation.city);
                }
            }



            var s = list.IsUkraine();


            Console.WriteLine();
            Console.WriteLine();
            Print(s.ToList());


            

            
            Console.ReadKey();
        }
    }
}
