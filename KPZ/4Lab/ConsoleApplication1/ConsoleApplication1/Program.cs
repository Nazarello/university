﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{

    enum SnakeType
    {
        piton,cobra,whyz
    }

    interface IMove
    {
        string GoLeft();
        string GoRight();
        string GoDown();
        string GoUp();
    }

    public interface ISpecialAbilities
    {
        string DoBite();
        string DoPoison();
    }

    abstract class ProgresiveMove : IMove
    {

        protected string poisonType;
        public string biteType;
        private string doIt;

        protected ProgresiveMove(string doIt)
        {
            this.doIt = doIt;
        }
        public string GoDown()
        {
            return "Snake go Down";
        }

        public string GoLeft()
        {
            return "Snake go Left";
        }

        public string GoRight()
        {
            return "Snake go Right";
        }

        public string GoUp()
        {
            return "Snake go UP";
        }

        public string DoIt()
        {
            return doIt;
        }

        public abstract string Jamp();
    }

    class Snake : ProgresiveMove , ISpecialAbilities
    {

        SnakeType type = (int)SnakeType.piton & (int)SnakeType.whyz;
        static bool die;
        static Snake()
        {
            die = false;
        }
        public Snake(string poisonType, string biteType, string doIt) : base(doIt)
        {
            die = false;
            this.poisonType = poisonType;
            this.biteType = biteType;
        }
        public Snake(SnakeType type,string poisonType,string biteType, string doIt) : this(poisonType,biteType,doIt)
        {
            this.type = type;           
        }

        public string DoBite()
        {
            return String.Format("I {0} you",biteType);
        }

        public string DoPoison()
        {
            return String.Format("I {0} you", poisonType);
        }

        public override string Jamp()
        {
            return "Snake Jamp";
        }

        

        protected class Nested
        {
            public void SomeMethod()
            {
                Console.WriteLine("Nested Method");
            }
        }        
    }
    class Program
    {
        static void Main(string[] args)
        {
            Snake shake = new Snake(SnakeType.cobra, "paralich", "die", "hide");
            int counts;
            while (!Int32.TryParse(Console.ReadLine(), out counts));

            object x = (object)counts;

            for (int i = 0; i < (int)x; i++)
            {
                string s = Console.ReadLine();
                switch (s)
                {
                    case "GoLeft":
                        Console.WriteLine(shake.GoLeft());
                        break;
                    case "GoRight":
                        Console.WriteLine(shake.GoRight());
                        break;
                    case "GoUp":
                        Console.WriteLine(shake.GoUp()) ;
                        break;
                    case "GoDown":
                        Console.WriteLine(shake.GoDown());
                        break;
                    default:
                        Console.WriteLine("Have not comand") ;
                        break;
                }
            }


            Console.ReadLine();
            

        }
    }
}
