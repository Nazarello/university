﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;

namespace Interpretator.Nazarii
{
    interface IPatternsInclude
    {
        void InitTempList();
        void InitBasePattern();
        void InitOutputPattern();
        void InitFileStreamPattern();
        void InitRegexPattern();
        void InitAll();
    }

    class PatternsInit : IPatternsInclude
    {
        List<string> list = new List<string>();
        List<string> tempList = new List<string>();

        public void InitTempList()
        { 
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*\"\\s*(?<value>[\\D\\d]+)\\s*\"\\s*;"); // var temp = "asdsd";
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*(?<token>GetTextFromFile)\\s*[(]'(?<fileName>[\\D\\d]*)'[)];"); // var temp = GetTextFromFile('C:\\Users\Nazar Beseniuk\Desktop\lol.txt');
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*(?<token>FindText)\\s*[(](?<arg1>\\s*\\w+\\s*),(?<arg2>\\s*\\w+\\s*)[)];"); //var bool = FindText(string,pattern);
            tempList.Add("\\s*(?<Method>(Output)|(Outputln))\\s*[(](?<variable>[\\D\\d]+)[)]\\s*"); //Output(x);
            tempList.Add("\\s*(?<Method>(Output)|(Outputln))\\s*[(]'(?<variable>[\\D\\d]+)'[)]\\s*"); //Output("asdfg");
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*(?<variable2>\\w+)\\s*;"); // var temp = x;
            tempList.Add("\\s*(?<variable>\\w+)\\s*=\\s*(?<variable2>\\w+)\\s*;"); // x = y;
            tempList.Add("\\s*StringReplace\\s*[(](?<arg1>\\w+),\'(?<arg2>[\\D\\d]+)\',\'(?<arg3>[\\D\\d]+)\'[)];"); // StringReplace(input,oldValue,newValue);
            tempList.Add("\\s*#(?<include>(output)|(stream)|(regex)|(all));"); //#output
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*ToUpperCase\\s*[(]'\\s*(?<arg1>[\\D\\d]+)\\s*'[)];"); //var t = ToUpperCase('qweqwe');
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*ToLowCase\\s*[(]'\\s*(?<arg1>[\\D\\d]+)\\s*'[)];"); //var t = ToUpperCase('qweqwe');
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*ToUpperCase\\s*[(]\\s*(?<arg1>[\\D\\d]+)\\s*[)];"); //var t = ToUpperCase(x);
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*ToLowCase\\s*[(]\\s*(?<arg1>[\\D\\d]+)\\s*[)];"); //var t = ToLowCase(x);
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*Remove\\s*[(](?<arg1>\\w+),'\\s*(?<arg2>[\\D\\d]+)\\s*'[)];");//var t = Remove(x,'s');
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*(?<token>FindText)\\s*[(](?<arg1>\\s*\\w+\\s*),'(?<arg2>\\s*\\D+\\s*)'[)];"); //var bool = FindText(string,'\\w+');
            tempList.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*Concat\\s*[(](?<arg1>[\\D\\d]+)\\s*,(?<arg2>[\\D\\d]+)[)];");//var t = Concat(x,y);

        }//Ініціалізація тимчасового ліста з патернами

        public void InitBasePattern()
        {
            list.Add("\\s*#(?<include>(output)|(stream)|(regex)|(all));");
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*(?<variable2>\\w+)\\s*;"); // var temp = x;
            list.Add("\\s*(?<variable>\\w+)\\s*=\\s*(?<variable2>\\w+)\\s*;"); // x = y;
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*\"\\s*(?<value>[\\D\\d]+)\\s*\"\\s*;"); // var temp = "asdsd";

        }//Ініціалізація базових патернів

        public void InitOutputPattern()
        {
            list.Add("\\s*(?<Method>(Output)|(Outputln))\\s*[(](?<variable>[\\D\\d]+)[)]\\s*"); //Output(x);
            list.Add("\\s*(?<Method>(Output)|(Outputln))\\s*[(]'(?<variable>[\\D\\d]+)'[)]\\s*"); //Output('fasd');
        }//Ініціалізація потернів для роботи з консолькою

        public void InitFileStreamPattern()
        {
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*(?<token>GetTextFromFile)\\s*[(]'(?<fileName>[\\D\\d]*)'[)];"); // var temp = GetTextFromFile('C:\\Users\\Nazar Beseniuk\\Desktop\\lol.txt');
        }//Ініціалізація патернів для роботи з файлами

        public void InitRegexPattern()
        {
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*(?<token>FindText)\\s*[(](?<arg1>\\s*\\w+\\s*),(?<arg2>\\s*\\w+\\s*)[)];"); //var bool = FindText(string,pattern);
            list.Add("\\s*StringReplace\\s*[(](?<arg1>\\w+),\'(?<arg2>[\\D\\d]+)\',\'(?<arg3>[\\D\\d]+)\'[)];"); // StringReplace(input,'oldValue','newValue');
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*ToUpperCase\\s*[(]'\\s*(?<arg1>[\\D\\d]+)\\s*'[)];"); //var t = ToUpperCase('qweqwe');
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*ToLowCase\\s*[(]'\\s*(?<arg1>[\\D\\d]+)\\s*'[)];"); //var t = ToUpperCase('qweqwe');
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*ToUpperCase\\s*[(]\\s*(?<arg1>[\\D\\d]+)\\s*[)];"); //var t = ToUpperCase(x);
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*ToLowCase\\s*[(]\\s*(?<arg1>[\\D\\d]+)\\s*[)];"); //var t = ToLowCase(x);
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*Remove\\s*[(](?<arg1>\\w+),'\\s*(?<arg2>[\\D\\d]+)\\s*'[)];");//var t = Remove(x,'s');
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*(?<token>FindText)\\s*[(](?<arg1>\\s*\\w+\\s*),'(?<arg2>\\s*\\D+\\s*)'[)];"); //var bool = FindText(string,"\\w+");
            list.Add("\\s*var\\s+(?<variable>\\w+)\\s*=\\s*Concat\\s*[(](?<arg1>[\\D\\d]+)\\s*,(?<arg2>[\\D\\d]+)[)];");//var t = Concat(x,y);


        }//Ініціалізація патренів для роботи з регулярними виразами

        public void InitAll()// Ініціалізація всіх поттернів
        {
            list.Clear();
            InitBasePattern();
            InitOutputPattern();
            InitFileStreamPattern();
            InitRegexPattern();
        }

        public List<string> GetTempList()
        {
            return tempList;
        }

        public List<string> GetList()
        {
            return list;
        }
    }

    
    public partial class MainWindow : Window
    {

        Hashtable  variable = new Hashtable();
        List<string> list = new List<string>();
        List<string> tempList = new List<string>();
        PatternsInit patterns;
        OutputConsole console;

        public MainWindow()
        {
            InitializeComponent();
            console = new OutputConsole();
            patterns = new PatternsInit();
            patterns.InitBasePattern();
            patterns.InitTempList();
            list = patterns.GetList();
            tempList = patterns.GetTempList();
        }

        private void CheckPutterns(string row)
        {
            int patternIndex = -1;
            int caseIndex = -1;
            string temp = "";

            for (int i = 0; i < list.Count; i++)
            {
                if (Regex.IsMatch(row, list[i]))//пошук патерна для нашого рядочка
                {
                    patternIndex = i;
                    break;
                }
            }

            
            for (int j = 0; j < tempList.Count; j++)
            {
                if (list[patternIndex] == tempList[j])//пошук патерна з нашого ліста
                {
                    caseIndex = j;
                    break;
                }
            }

            if (caseIndex == -1 || patternIndex == -1)
                throw new Exception("Cant find pattern");

            Choose(caseIndex,row);//виклик функції виконання коду
          
        }

        private void run_Click(object sender, RoutedEventArgs e)
        {
            errorText.Text = "";
            string text = inputText.Text;
            string[] rows = text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);//розподіл нашого текста

            for (int i = 0; i < rows.Length; i++)
            {
                try
                {

                    CheckPutterns(rows[i]);

                }
                catch (Exception er)
                {
                    errorText.Text = er.Message + "on line" + i;
                }
            }
            if (console.IsActive)
                console.Close();
            if (!console.IsActive)
               console.Show();
            variable.Clear();

        }

        public void Choose(int caseIndex,string row)
        {
            var m = Regex.Match(row, tempList[caseIndex]);
            switch (caseIndex)
            {
                case 0:
                    {
                        if (variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Variable already declarate");
                        variable.Add(m.Groups["variable"].Value, m.Groups["value"].Value);
                    }
                    break;
                case 1:
                    {
                        if (variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Variable already declarate");

                        if (!File.Exists(m.Groups["fileName"].Value.ToString()))
                            throw new Exception("File not exist!");

                        StreamReader sr = new StreamReader(m.Groups["fileName"].Value.ToString());
                        string text = sr.ReadToEnd();
                        sr.Close();
                        variable.Add(m.Groups["variable"].Value, text);
                    }
                    break;
                case 2:
                    {
                        if (!variable.Contains(m.Groups["arg1"].Value))
                            throw new Exception("Undefine variable 1");

                        if (!variable.Contains(m.Groups["arg2"].Value))
                            throw new Exception("Undefine variable 2");

                        var match = Regex.IsMatch(variable[m.Groups["arg1"].Value].ToString(), variable[m.Groups["arg2"].Value].ToString());

                        variable.Add(m.Groups["variable"].Value, match.ToString());
                    }
                    break;
                case 3:
                    {

                        //if (!variable.Contains(m.Groups["variable"].Value))
                        //    throw new Exception("Undefine variable");
                        if (variable.Contains(m.Groups["variable"].Value))
                        {
                            if (m.Groups["Method"].Value.ToString() == "Output")
                                console.resText.Text += variable[m.Groups["variable"].Value].ToString();
                            if (m.Groups["Method"].Value.ToString() == "Outputln")
                                console.resText.Text += variable[m.Groups["variable"].Value].ToString() + "\n";
                        }
                        else
                        {
                            if (m.Groups["Method"].Value.ToString() == "Output")
                                console.resText.Text += m.Groups["variable"].Value.ToString().Replace("'","");
                            if (m.Groups["Method"].Value.ToString() == "Outputln")
                                console.resText.Text += m.Groups["variable"].Value.ToString().Replace("'", "") + "\n";
                        }
                    }
                    break;
                case 4:
                    {

                        if (m.Groups["Method"].Value.ToString() == "Output")
                            console.resText.Text += m.Groups["variable"].Value.ToString();
                        if (m.Groups["Method"].Value.ToString() == "Outputln")
                            console.resText.Text += m.Groups["variable"].Value.ToString() + "\n";
                    }
                    break;
                case 5:
                    {
                        if (!variable.Contains(m.Groups["variable2"].Value))
                            throw new Exception("Undefine variable 1");

                        variable.Add(m.Groups["variable"].Value, null);

                        //if (!variable.Contains(m.Groups["variable2"].Value))
                        //    throw new Exception("Undefine variable 2");


                        variable[m.Groups["variable"].Value] = variable[m.Groups["variable2"].Value];
                    }
                    break;
                case 6:
                    {
                        if (!variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Undefine variable 1");

                        if (!variable.Contains(m.Groups["variable2"].Value))
                            throw new Exception("Undefine variable 2");

                        variable[m.Groups["variable"].Value] = variable[m.Groups["variable2"].Value];
                    }
                    break;
                case 7:
                    {
                        if (!variable.Contains(m.Groups["arg1"].Value))
                            throw new Exception("Undefine variable 1");

                        string arg1 = variable[m.Groups["arg1"].Value].ToString();

                        arg1 = arg1.Replace(m.Groups["arg2"].Value.ToString(), m.Groups["arg3"].Value.ToString());
                        variable[m.Groups["arg1"].Value] = arg1;
                    }
                    break;
                case 8:
                    {
                        if (m.Groups["include"].Value == "all")
                        {
                            patterns.InitAll();
                            list = patterns.GetList();
                        }
                        if (m.Groups["include"].Value == "output")
                        {
                            patterns.InitOutputPattern();
                            list = patterns.GetList();
                        }
                        if (m.Groups["include"].Value == "stream")
                        {
                            patterns.InitFileStreamPattern();
                            list = patterns.GetList();
                        }
                        if (m.Groups["include"].Value == "regex")
                        {
                            patterns.InitRegexPattern();
                            list = patterns.GetList();
                        }
                    }
                    break;
                case 9:
                    {
                        if (variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Undefine variable 1");

                        string arg1 = m.Groups["arg1"].Value.ToString();

                        arg1 = arg1.ToUpper();
                        variable.Add(m.Groups["variable"].Value, arg1);
                    }
                    break;
                case 10:
                    {
                        if (variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Undefine variable 1");

                        string arg1 = m.Groups["arg1"].Value.ToString();

                        arg1 = arg1.ToLower();
                        variable.Add(m.Groups["variable"].Value, arg1);
                    }
                    break;
                case 11:
                    {
                        if (variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Undefine variable 1");
                        if (!variable.Contains(m.Groups["arg1"].Value))
                            throw new Exception("Undefine argument");

                        string arg1 = variable[m.Groups["arg1"].Value].ToString();

                        arg1 = arg1.ToUpper();
                        variable.Add(m.Groups["variable"].Value, arg1);
                    }
                    break;
                case 12:
                    {
                        if (variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Undefine variable 1");
                        if (!variable.Contains(m.Groups["arg1"].Value))
                            throw new Exception("Undefine argument");

                        string arg1 = variable[m.Groups["arg1"].Value].ToString();

                        arg1 = arg1.ToLower();
                        variable.Add(m.Groups["variable"].Value, arg1);
                    }
                    break;
                //var t = Remove(x,'s');
                case 13:
                    {
                        if (variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Undefine variable 1");
                        if (!variable.Contains(m.Groups["arg1"].Value))
                            throw new Exception("Undefine argument");

                        string arg1 = variable[m.Groups["arg1"].Value].ToString().Replace(m.Groups["arg2"].Value, "");
                        variable.Add(m.Groups["variable"].Value, arg1);
                    }
                    break;
                case 14:
                    {
                        if (!variable.Contains(m.Groups["arg1"].Value))
                            throw new Exception("Undefine argument 1");


                        var match = Regex.IsMatch(variable[m.Groups["arg1"].Value].ToString(), m.Groups["arg2"].Value.ToString());

                        variable.Add(m.Groups["variable"].Value, match.ToString());
                    }
                    break;
                //var t = Concat(x,'y');
                case 15:
                    {
                        if (!variable.Contains(m.Groups["arg1"].Value))
                            throw new Exception("Undefine argument 1");
                        if (variable.Contains(m.Groups["variable"].Value))
                            throw new Exception("Variable already declarate");

                        if (!variable.Contains(m.Groups["arg2"].Value))
                        {
                            variable.Add(m.Groups["variable"].Value, variable[m.Groups["arg1"].Value].ToString() + m.Groups["arg2"].Value);
                        }
                        else
                        {
                            variable.Add(m.Groups["variable"].Value, variable[m.Groups["arg1"].Value].ToString() + variable[m.Groups["arg2"].Value].ToString());
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
