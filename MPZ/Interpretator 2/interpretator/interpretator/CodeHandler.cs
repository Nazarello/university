﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace interpretator
{
    public static class CodeHandler
    {
        public static void FindText(string variable, string text, ref Output output)
        {
            bool find;
            int lastTextIndex = 0;
            string tmp;
            for (int i = 0; i < variable.Length - text.Length + 1; i++)
            {
                find = true;
                for (int j = 0; j < text.Length; j++)
                {                    
                    if (variable[i + j] != text[j])
                    {
                        find = false;
                        break;
                    }
                }
                if (find)
                {
                    tmp = variable.Substring(lastTextIndex, i - lastTextIndex);
                    output.rtbOutput.AppendText(tmp, Color.Black);
                    output.rtbOutput.AppendText(text, Color.Red);
                    lastTextIndex = i + text.Length;
                }
            }
            tmp = variable.Substring(lastTextIndex, variable.Length - lastTextIndex);
            output.rtbOutput.AppendText(tmp, Color.Black);
        }

        public static void FindAndReplace(string variable, string findtext, string replacetext, ref Output output)
        {
            bool find;
            int lastTextIndex = 0;
            string tmp;
            for (int i = 0; i < variable.Length - findtext.Length; i++)
            {
                find = true;
                for (int j = 0; j < findtext.Length; j++)
                {
                    if (variable[i + j] != findtext[j])
                    {
                        find = false;
                    }
                }
                if (find)
                {
                    tmp = variable.Substring(lastTextIndex, i - lastTextIndex);
                    output.rtbOutput.AppendText(tmp, Color.Black);
                    output.rtbOutput.AppendText(replacetext, Color.Red);
                    lastTextIndex = i + findtext.Length;
                }
            }
            tmp = variable.Substring(lastTextIndex, variable.Length - lastTextIndex);
            output.rtbOutput.AppendText(tmp, Color.Black);
        }
    }
   
}
