﻿using interpretator;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace interpretator
{
    
    public partial class MainForm : Form
    {
        public Hashtable variables = new Hashtable();
        public List<string> patternList = new List<string>();
        public bool stream, console, search;
        public Output output = new Output();

        public MainForm()
        {
            InitializeComponent();
            patternList.Add("(?<varname>[a-zA-Z]\\w*)\\s*=\\s*(?<value>\\w+)"); // str = asdfghj
            patternList.Add("(?<varname>[a-zA-Z]\\w*)\\s*=\\s*'(?<filename>[\\D\\d]+)'"); // str = 'text.txt'
            patternList.Add("Find[(](?<value>\\w+)[)]\\s+in\\s+(?<varname>[a-zA-Z]\\w*)"); // Find(smth) in str
            patternList.Add("Find[(](?<value>\\w+)[)]\\s+in\\s+'(?<filename>[\\D\\d]+)'"); // Find(smth) in 'text.txt'
            patternList.Add("FindAndReplace[(](?<findtext>\\w+),\\s*(?<replacetext>\\w+)[)]\\s+in\\s+(?<varname>[a-zA-Z]\\w*)"); //FindAndReplace(muur, meow) in str
            patternList.Add("FindAndReplace[(](?<findtext>\\w+),\\s*(?<replacetext>\\w+)[)]\\s+in\\s+'(?<filename>[\\D\\d]+)'"); //FindAndReplace(muur, meow) in 'text.txt'
            patternList.Add("Show\\s+(?<varname>[a-zA-Z]\\w*)"); //Show str
            patternList.Add("Show\\s+'(?<filename>[\\D\\d]+)'"); //Show 'text.txt'
            patternList.Add("Say\\s+(?<value>\\w+)"); // Say sdfghjkl
            patternList.Add("Add\\s+all");
            patternList.Add("Add\\s+stream");
            patternList.Add("Add\\s+console");
            patternList.Add("Add\\s+search");
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            output.rtbOutput.Text = "";
            string text = rtbInput.Text;
            CheckCode(text);
        }

        public void CheckCode(string input)
        {
            string[] rows = input.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            rtbErrors.Text = "";
            bool correct = false;
            for (int i = 0; i < rows.Length; i++)
            {
                try
                {
                    for (int j = 0; j < patternList.Count; j++)
                    {
                        if (Regex.IsMatch(rows[i], patternList[j]))
                        {
                            correct = true;
                            CheckRow(rows[i], i, j);
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    correct = false;
                    rtbErrors.Text += "Помилка у " + (i + 1) + " рядку.\n" + e.Message;
                }
            }
            if (correct)
            {
                output.Show();
                return;
            }
            rtbErrors.Text += "Помилка компіляції";
        }
        private void CheckRow(string row, int rowNumber, int patternIndex)
        {
            rowNumber++;
            string varname, filename, value, findtext, replacetext;
            var m = Regex.Match(row, patternList[patternIndex]);
            switch (patternIndex)
            {
                // str = asdfghj
                case 0:
                    varname = m.Groups["varname"].Value;
                    value = m.Groups["value"].Value;
                    
                    if (!variables.Contains(varname)
                        )
                    {
                        variables.Add(varname, value);
                    }
                    else
                    {
                        variables[varname] = value;
                    }
                    break;
                // str = 'text.txt'
                case 1:
                    if (!stream)
                    {
                        throw new Exception("Бібліотека stream не підключена.\n");
                    }
                    varname = m.Groups["varname"].Value;
                    filename = m.Groups["filename"].Value;
                    
                    if (!File.Exists(filename.ToString()))
                    {
                        throw new Exception("Файл " + filename + " не знайдено.\n");
                    }

                    if (!variables.Contains(varname))
                    {
                        StreamReader sr = new StreamReader(filename.ToString());
                        string text = sr.ReadToEnd();
                        //rtbErrors.AppendText(text, Color.Black);
                        sr.Close();
                        variables.Add(varname, text);
                    }
                    else
                    {
                        StreamReader sr = new StreamReader(filename.ToString());
                        string text = sr.ReadToEnd();
                        sr.Close();
                        variables[varname] = text;
                    }
                    break;
                // Find(smth) in str
                case 2:
                    if (!search)
                    {
                        throw new Exception("Бібліотека search не підключена.\n");
                    }
                    value = m.Groups["value"].Value;
                    varname = m.Groups["varname"].Value;
                    if (variables.Contains(varname))
                    {
                        CodeHandler.FindText(variables[varname].ToString(), value, ref output);
                    }
                    else
                    {
                        throw new Exception("Змінної " + varname + " не існує.\n");
                    }
                    break;
                // Find(smth) in 'text.txt'
                case 3:
                    if (!stream)
                    {
                        throw new Exception("Бібліотека stream не підключена.\n");
                    }
                    if (!search)
                    {
                        throw new Exception("Бібліотека search не підключена.\n");
                    }
                    filename = m.Groups["filename"].Value;
                    value = m.Groups["value"].Value;
                    if (File.Exists(filename))
                    {
                        StreamReader sr = new StreamReader(filename);
                        string text = sr.ReadToEnd();
                        sr.Close();
                        CodeHandler.FindText(text, value, ref output);
                    }
                    else
                    {
                        throw new Exception("Файл " + filename + " не знайдено.\n");
                    }
                   
                    break;
                //FindAndReplace(muur, meow) in str
                case 4:
                    if (!search)
                    {
                        throw new Exception("Бібліотека search не підключена.\n");
                    }
                    findtext = m.Groups["findtext"].Value;
                    replacetext = m.Groups["replacetext"].Value;
                    varname = m.Groups["varname"].Value;
                    if (variables.Contains(varname))
                    {
                        CodeHandler.FindAndReplace(variables[varname].ToString(), findtext, replacetext, ref output);
                    }
                    else
                    {
                        throw new Exception("Змінної " + varname + " не існує.\n");
                    }
                    break;
                //FindAndReplace(muur, meow) in 'text.txt'
                case 5:
                    if (!stream)
                    {
                        throw new Exception("Бібліотека stream не підключена.\n");
                    }
                    if (!search)
                    {
                        throw new Exception("Бібліотека search не підключена.\n");
                    }
                    findtext = m.Groups["findtext"].Value;
                    replacetext = m.Groups["replacetext"].Value;
                    filename = m.Groups["filename"].Value;
                    if (File.Exists(filename))
                    {
                        StreamReader sr = new StreamReader(filename);
                        string text = sr.ReadToEnd();
                        sr.Close();
                        CodeHandler.FindAndReplace(text, findtext, replacetext, ref output);
                    }
                    else
                    break;
                    {
                        throw new Exception("Файл " + filename + " не знайдено.\n");
                    }
                //Show str
                case 6:
                    varname = m.Groups["varname"].Value;
                    if (!console)
                    {
                        throw new Exception("Бібліотека console не підключена.\n");
                    }
                    
                    if (variables.Contains(varname))
                    {
                        output.rtbOutput.AppendText(variables[varname].ToString(), Color.Black);
                    }
                    else
                    {
                        throw new Exception("Змінної " + varname + " не існує.\n");
                    }
                    break;
                //Show 'text.txt'
                case 7:
                    filename = m.Groups["filename"].Value;
                    varname = m.Groups["varname"].Value;
                    if (!stream)
                    {
                        throw new Exception("Бібліотека stream не підключена.\n");
                    }
                    if (!console)
                    {
                        throw new Exception("Бібліотека console не підключена.\n");
                    }
                    if (File.Exists(filename))
                    {
                        StreamReader sr = new StreamReader(filename);
                        string text = sr.ReadToEnd();
                        sr.Close();
                        output.rtbOutput.AppendText(text, Color.Black);
                    }
                    else
                    {
                        throw new Exception("Файл " + filename + " не знайдено.\n");
                    }
                    break;
                // Say sdfghjkl
                case 8:
                    if (!console)
                    {
                        throw new Exception("Бібліотека console не підключена.\n");
                    }
                    value = m.Groups["value"].Value;
                    output.rtbOutput.AppendText(value, Color.Black);
                    break;
                // Add all
                case 9:
                    stream = true;
                    console = true;
                    search = true;
                    break;
                // Add stream
                case 10:
                    stream = true;
                    break;
                // Add console
                case 11:
                    console = true;
                    break;
                // Add search
                case 12:
                    search = true;
                    break;

            }
        }

    }
    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;
            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
